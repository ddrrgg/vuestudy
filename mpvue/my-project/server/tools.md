# 腾讯云小程序解决方案 Demo 工具使用文档

#本地开发环境的话，  先修改配置文件config.json  
```
  serverHost:"localhost",
    tunnelServerUrl:'',

    tunnelSignatureKey:'',

    qcloudAppId:'1256211251',
    
    qcloudSecretId:'AKIDnOjpgIZ7ye6JH0urvjHadQzIL8fwboOi',
    qcloudSecretKey:'DBsKWS8YihQt62i7JFxQggPt8EZliSxE',

    wxMessageToken:'weixinmsgtoken',
    networkTimeout:3000,


    //注意mysql  数据的密码    
```

本文件夹下的脚本为腾讯云小程序解决方案 Demo 配套的工具，旨在让用户方便快捷的使用并创建小程序的开发环境。

工具包括：

- [数据库初始化工具](#数据库初始化工具)

## 数据库初始化工具

本工具是为了让用户快速的按照腾讯云制定的数据库 schema 创建符合 SDK 标准的数据库结构。

_**注意**：本工具支持的 MySQL 版本为 **5.7**，并且需提前在数据库中创建名为 `cAuth` 的数据库。`charset` 设置为 `utf8mb4`。_

快速使用：

```bash
npm  install
npm run initdb
```

或直接执行 `tools` 目录下的 `initdb.js` 文件：

```bash
# 请保证已经执行了 npm install 安装了所需要的依赖
node tools/initdb.js
```

我们提供了初始化的 SQL 文件，你也可以用其他数据库工具（如 Navicat）直接导入 SQL 文件。


### 安装本地调试环境的工具  自动更新

```
  npm install -g nodemon  老是安装失败  
  npm remove g nodemon 

  重新执行
  npm install nodemon

  npm run dev
```

###  浏览器访问
```
端口在config.js指定
localhost:5757/weapp/demo
```

## server 目录下执行

```
node tools/initdb.js
将数据表插入到 cauth表里
```