/* eslint-disable */ 

// 工具库  上面的是或略检查

import config from './config'

export function get (url) {
  return new Promise((resolve, reject) => {
    wx.request({
      url: config.host + url,
      success: function (res) {
        if (res.data.coe == 0) {
            resolve(res.data.data)
        } else {
            resolve(res.data)
        }
      }
    })
  })
}


export function showSuccess(text){
  wx.showToast({
    title:text,
    icon:'success'
  })
}